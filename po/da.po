# Danish translation for gnome-connections.
# Copyright (C) 2020, 2024 gnome-connections's COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-connections package.
# scootergrisen, 2020-2021.
# Ask Hjorth Larsen <asklarsen@gmail.com>, 2022-2024.
msgid ""
msgstr ""
"Project-Id-Version: gnome-connections master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/connections/issues\n"
"POT-Creation-Date: 2024-08-31 14:13+0000\n"
"PO-Revision-Date: 2024-09-08 17:35+0200\n"
"Last-Translator: Ask Hjorth Larsen <asklarsen@gmail.com>\n"
"Language-Team: Danish <dansk@dansk-gruppen.dk>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: data/org.gnome.Connections.appdata.xml.in:7
#: data/org.gnome.Connections.desktop.in:3 src/application.vala:92
#: src/ui/topbar.ui:9 src/ui/window.ui:5 src/window.vala:115
msgid "Connections"
msgstr "Forbindelser"

#: data/org.gnome.Connections.appdata.xml.in:8
msgid "View and use other desktops"
msgstr "Vis og brug andre skriveborde"

#: data/org.gnome.Connections.appdata.xml.in:10
msgid ""
"Connections allows you to connect to and use other desktops. This can be a "
"great way to access content or software on a different desktop operating "
"system. It can also be used as a way to provide support to users who might "
"need help."
msgstr ""
"Forbindelser lader dig forbinde til og bruge andre skriveborde. Dette kan "
"være en god måde at tilgå data eller programmer på et andet "
"skrivebordsoperativsystem. Programmet kan også bruges til at hjælpe andre "
"brugere."

#: data/org.gnome.Connections.appdata.xml.in:13
msgid ""
"A range of different operating systems can be connected to, including Linux "
"and Windows desktops. You can also connect to virtual machines."
msgstr ""
"Det er muligt at forbinde til en række forskellige operativsystemer "
"inklusive Linux- og Windowsskriveborde. Du kan også forbinde til virtuelle "
"maskiner."

#: data/org.gnome.Connections.appdata.xml.in:16
msgid ""
"Connections uses the widely supported VNC and RDP protocols, and one of "
"these must be enabled on the desktop that you want to connect to."
msgstr ""
"Forbindelser bruger de bredt understøttede protokoller VNC og RDP. En af "
"disse skal være slået til på det skrivebord, du vil forbinde til."

#. developer_name tag deprecated with Appstream 1.0
#: data/org.gnome.Connections.appdata.xml.in:44
msgid "The GNOME Project"
msgstr "GNOME-projektet"

#: data/org.gnome.Connections.desktop.in:8
msgid "vnc;rdp;remote;desktop;windows;support;access;view"
msgstr ""
"vnc;rdp;fjern;remote;desktop;;skrivebord;windows;support;hjælp;adgang;"
"tilgang;tilgå;vis"

#: data/org.gnome.Connections.xml:5
msgid "Remote Desktop (VNC) file"
msgstr "Fjernskrivebordsfil (VNC)"

#: src/actions-popover.vala:44
msgid "Delete"
msgstr "Slet"

#: src/actions-popover.vala:48 src/topbar.vala:66 src/ui/topbar.ui:240
msgid "Properties"
msgstr "Egenskaber"

#: src/application.vala:88
msgid "translator-credits"
msgstr ""
"scootergrisen\n"
"Ask Hjorth Larsen\n"
"\n"
"Dansk-gruppen\n"
"Websted http://dansk-gruppen.dk\n"
"E-mail <dansk@dansk-gruppen.dk>"

#: src/application.vala:89 src/application.vala:242
msgid "A remote desktop client for the GNOME desktop environment"
msgstr "En fjernskrivebordsklient til GNOME-skrivebordsmiljøet"

#: src/application.vala:151
#, c-format
msgid "Couldn’t open file of unknown mime type %s"
msgstr "Kunne ikke åbne fil af ukendt MIME-type %s"

#: src/application.vala:182
#, c-format
msgid "Connection to “%s” has been deleted"
msgstr "Forbindelsen til “%s” er blevet slettet"

#: src/application.vala:185
msgid "Undo"
msgstr "Fortryd"

#: src/application.vala:232
msgid "URL to connect"
msgstr "URL som der skal oprettes forbindelse til"

#: src/application.vala:233
msgid "Open .vnc or .rdp file at the given PATH"
msgstr "Åbn .vnc- eller .rdp-fil på den angivne STI"

#: src/application.vala:233
msgid "PATH"
msgstr "STI"

#: src/application.vala:234
msgid "Open in full screen"
msgstr "Åbn i fuldskærm"

#: src/application.vala:257
msgid "Too many command-line arguments specified.\n"
msgstr "Der er angivet for mange kommandolinjeargumenter.\n"

#. Translators: %s => the timestamp of when the screenshot was taken.
#: src/connection.vala:70
#, c-format
msgid "Screenshot from %s"
msgstr "Skærmbillede fra %s"

#: src/connection.vala:85
msgid "Screenshot taken"
msgstr "Skærmbillede taget"

#. Translators: Open is a verb
#: src/connection.vala:88
msgid "Open"
msgstr "Åbn"

#: src/connection.vala:205
#, c-format
msgid "Authentication failed: %s"
msgstr "Godkendelse mislykkedes: %s"

#: src/dialogs.vala:167
#, c-format
msgid ""
"Connecting to “%s” for the first time. To be sure you're connecting to the "
"machine it claims to be, please verify the fingerprints match. This process "
"is only done once."
msgstr ""
"Forbinder til “%s” for første gang. For at sikre at du forbinder til den "
"maskine, som maskinen påstår at være, så kontrollér venligst, at "
"fingeraftrykkene stemmer. Denne proces udføres kun én gang."

#: src/dialogs.vala:201
#, c-format
msgid ""
"The remote server “%s” certificate doesn't match local copy. It may be "
"someone's pretending to be the server."
msgstr ""
"Certifikatet for fjernserveren “%s” svarer ikke til den lokale kopi. Nogen "
"foregiver måske at være serveren."

#: src/dialogs.vala:241
#, c-format
msgid ""
"The remote server “%s” requires a username and password to continue to "
"connect."
msgstr ""
"Fjernserveren “%s” kræver et brugernavn og en adgangskode for at fortsætte "
"med at forbinde."

#. Translators: Showing size of widget as WIDTH×HEIGHT here.
#: src/display-view.vala:131
#, c-format
msgid "%d×%d"
msgstr "%d×%d"

#: src/onboarding-dialog.vala:96 src/ui/onboarding-dialog.ui:148
msgid "_No Thanks"
msgstr "_Nej tak"

#: src/onboarding-dialog.vala:96
msgid "_Close"
msgstr "_Luk"

#. Translators: Combo item for resizing remote desktop to window's size
#: src/rdp-preferences-window.vala:49 src/ui/vnc-preferences.ui:107
msgid "Resize desktop"
msgstr "Ændr størrelse på skrivebord"

#: src/topbar.vala:58 src/ui/topbar.ui:236
msgid "Take Screenshot"
msgstr "Tag skærmbillede"

#: src/topbar.vala:62 src/ui/topbar.ui:198
msgid "Fullscreen"
msgstr "Fuldskærm"

#: src/ui/assistant.ui:26
msgid "Enter the network identifier of the remote desktop to connect to:"
msgstr "Indtast netværks-id for fjernskrivebordet, du vil forbinde til:"

#: src/ui/assistant.ui:53
msgid "Connection Type"
msgstr "Type af forbindelse"

#: src/ui/assistant.ui:63
msgid "RDP (standard for connecting to Windows)"
msgstr "RDP (standard for forbindelser til Windows)"

#: src/ui/assistant.ui:72
msgid "VNC (standard for connecting to Linux)"
msgstr "VNC (standard for forbindelser til Linux)"

#: src/ui/assistant.ui:90 src/ui/topbar.ui:225
msgid "Help"
msgstr "Hjælp"

#: src/ui/assistant.ui:100 src/ui/dialog.ui:68 src/ui/dialog.ui:74
msgid "Connect"
msgstr "Forbind"

#: src/ui/auth-notification.ui:39
msgid "_Username"
msgstr "_Brugernavn"

#: src/ui/auth-notification.ui:71
msgid "_Password"
msgstr "_Adgangskode"

#: src/ui/auth-notification.ui:108
msgid "Sign In"
msgstr "Log ind"

#. Translators: The dialog with this title is shown to the user before opening the connection if needed (e.g. certificate confirmation).
#: src/ui/dialog.ui:18
msgid "Open Connection"
msgstr "Åbn forbindelse"

#: src/ui/dialog.ui:37 src/ui/dialog.ui:43
msgid "Verify"
msgstr "Kontrollér"

#: src/ui/dialog.ui:105 src/ui/dialog.ui:613
msgid "Cancel"
msgstr "Annuller"

#: src/ui/dialog.ui:144
msgid "Verify Fingerprint"
msgstr "Kontrollér fingeraftryk"

# Strengen "Verify encryption" kommer fra gnome-control-center
#: src/ui/dialog.ui:211
msgid ""
"On Windows run certlm.msc or certmgr.msc\n"
"and copy the certificate from\n"
"<b>Remote Desktop>Certificates</b> to a file.\n"
"Then obtain its fingerprint via\n"
"\"certutil.exe -hashfile file SHA256\".\n"
"\n"
"If GNOME is used on the connected endpoint,\n"
"the fingerprint can be verified in the <b>Settings>Sharing</b>\n"
"panel or <b>Settings>System</b> panel under\n"
"<b>Remote Desktop>Verify Encryption</b>."
msgstr ""
"På Windows kan du køre certlm.msc eller certmgr.msc\n"
"og kopiere certifikatet fra\n"
"<b>Fjernskrivebord>Certifikater</b> til en fil.\n"
"Find så dens fingeraftryk via\n"
"“certutil.exe -hashfile fil SHA256”.\n"
"\n"
"Hvis GNOME bruges på det forbundne slutpunkt,\n"
"kan fingeraftrykket kontrolleres i panelet <b>Indstillinger>Deling</b>\n"
"eller panelet <b>Indstillinger>System</b> under\n"
"<b>Fjernskrivebord>Bekræft kryptering</b>."

#: src/ui/dialog.ui:292
msgid "Unexpected Certificate"
msgstr "Uventet certifikat"

#: src/ui/dialog.ui:334
msgid "Delete Local Certificate"
msgstr "Slet lokalt certifikat"

#: src/ui/dialog.ui:387
msgid "Authentication Required"
msgstr "Kræver godkendelse"

#: src/ui/dialog.ui:438
msgid "Username…"
msgstr "Brugernavn …"

#: src/ui/dialog.ui:466
msgid "Password…"
msgstr "Adgangskode …"

#: src/ui/dialog.ui:516
msgid "Domain…"
msgstr "Domæne …"

#: src/ui/dialog.ui:537
msgid ""
"Domain field is commonly optional and can be\n"
"left empty."
msgstr ""
"Domænefeltet er normalt valgfrit og\n"
"kan stå tomt."

#: src/ui/dialog.ui:639
msgid "Authenticate"
msgstr "Godkend"

#: src/ui/onboarding-dialog.ui:42 src/ui/window.ui:51
msgid "Welcome to Connections"
msgstr "Velkommen til Forbindelser"

#: src/ui/onboarding-dialog.ui:43
msgid "Click next to learn about remote desktop."
msgstr "Klik næste for at se flere oplysninger om fjernskrivebord."

#: src/ui/onboarding-dialog.ui:50
msgid "Access other desktops"
msgstr "Tilgå andre skriveborde"

#: src/ui/onboarding-dialog.ui:51
msgid ""
"Connections allows viewing the screens of other desktops. They can also be "
"controlled using the pointer and keyboard."
msgstr ""
"Forbindelser kan vise skærmen fra andre skriveborde. De kan også styres med "
"markør og tastatur."

#: src/ui/onboarding-dialog.ui:58
msgid "Connect to different operating systems"
msgstr "Forbind til forskellige operativsystemer"

#: src/ui/onboarding-dialog.ui:59
msgid "You can access Linux, Mac, and Windows desktops."
msgstr "Du kan tilgå Linux-, Mac- og Windowsskriveborde."

#: src/ui/onboarding-dialog.ui:66
msgid "Setup is required"
msgstr "Kræver opsætning"

#: src/ui/onboarding-dialog.ui:67
msgid ""
"Remote desktop typically needs to be enabled on the computer you want to "
"connect to."
msgstr ""
"Fjernskrivebord skal typisk være slået til på den computer, du vil forbinde "
"til."

#: src/ui/onboarding-dialog.ui:74
msgid "Start by creating your first connection"
msgstr "Begynd med at oprette din første forbindelse"

#: src/ui/onboarding-dialog.ui:75
msgid ""
"To begin, you will need the address of the computer you want to connect to."
msgstr ""
"Til at begynde med får du brug for adressen pâ den maskine, du vil forbinde "
"til."

#: src/ui/rdp-preferences.ui:9 src/ui/vnc-preferences.ui:9
msgid "Connection preferences"
msgstr "Indstillinger for forbindelse"

#: src/ui/rdp-preferences.ui:22 src/ui/vnc-preferences.ui:22
msgid "Address"
msgstr "Adresse"

#: src/ui/rdp-preferences.ui:37 src/ui/vnc-preferences.ui:37
msgid "Name"
msgstr "Navn"

#: src/ui/rdp-preferences.ui:53 src/ui/vnc-preferences.ui:108
msgid "Fit window"
msgstr "Tilpas vindue"

#: src/ui/rdp-preferences.ui:54 src/ui/vnc-preferences.ui:109
msgid "Original size"
msgstr "Oprindelig størrelse"

#: src/ui/topbar.ui:21
msgid "New"
msgstr "Ny"

#: src/ui/topbar.ui:42
msgid "Application Menu"
msgstr "Programmenu"

#: src/ui/topbar.ui:70
msgid "Search"
msgstr "Søg"

#: src/ui/topbar.ui:104
msgid "Go Back"
msgstr "Gå tilbage"

#: src/ui/topbar.ui:125
msgid "Display Menu"
msgstr "Vis menu"

#: src/ui/topbar.ui:149
msgid "Disconnect"
msgstr "Afbryd"

#: src/ui/topbar.ui:174
msgid "Keyboard shortcuts"
msgstr "Tastaturgenveje"

#: src/ui/topbar.ui:221
msgid "Keyboard Shortcuts"
msgstr "Tastaturgenveje"

#: src/ui/topbar.ui:229
msgid "About Connections"
msgstr "Om Forbindelser"

#: src/ui/topbar.ui:254
msgid "Ctrl + Alt + Backspace"
msgstr "Ctrl + Alt + Backspace"

#: src/ui/topbar.ui:261
msgid "Ctrl + Alt + Del"
msgstr "Ctrl + Alt + Del"

#: src/ui/topbar.ui:268
msgid "Ctrl + Alt + F1"
msgstr "Ctrl + Alt + F1"

#: src/ui/topbar.ui:275
msgid "Ctrl + Alt + F2"
msgstr "Ctrl + Alt + F2"

#: src/ui/topbar.ui:282
msgid "Ctrl + Alt + F3"
msgstr "Ctrl + Alt + F3"

#: src/ui/topbar.ui:289
msgid "Ctrl + Alt + F7"
msgstr "Ctrl + Alt + F7"

#: src/ui/vnc-preferences.ui:54
msgid "Display"
msgstr "Visning"

#: src/ui/vnc-preferences.ui:59
msgid "View only"
msgstr "Kun visning"

#: src/ui/vnc-preferences.ui:66
msgid "Show local pointer"
msgstr "Vis lokal markør"

#: src/ui/vnc-preferences.ui:73
msgid "Enable audio"
msgstr "Slå lyden til"

#: src/ui/vnc-preferences.ui:81
msgid "Bandwidth"
msgstr "Båndbredde"

#: src/ui/vnc-preferences.ui:88
msgid "High quality"
msgstr "Høj kvalitet"

#: src/ui/vnc-preferences.ui:89
msgid "Fast refresh"
msgstr "Hurtig opdatering"

#: src/ui/vnc-preferences.ui:100
msgid "Scale mode"
msgstr "Skaleringstilstand"

#: src/ui/window.ui:52
msgid "Click the add button to create your first connection"
msgstr "Tryk blot på tilføjelsesknappen for at oprette din første forbindelse"

#: src/vnc-connection.vala:150
msgid "Couldn’t parse the file"
msgstr "Kunne ikke fortolke filen"

#. Translators: %s is a VNC file key
#: src/vnc-connection.vala:158 src/vnc-connection.vala:163
#: src/vnc-connection.vala:168 src/vnc-connection.vala:173
#, c-format
msgid "VNC File is missing key “%s”"
msgstr "VNC-filen mangler nøglen “%s”"

#, c-format
#~ msgid "“%s” requires authentication"
#~ msgstr "“%s” kræver godkendelse"

#~ msgid "Learn about how Connections works."
#~ msgstr "Lær om hvordan Forbindelser fungerer."

#~ msgid "Use other desktops, remotely"
#~ msgstr "Brug andre skriveborde over netværk"

#~ msgid "Enable remote desktop before connecting"
#~ msgstr "Slå fjernskrivebord til inden forbindelse oprettes"

#~ msgid ""
#~ "Computers need to be set up for remote desktop before you can connect to "
#~ "them."
#~ msgstr ""
#~ "Computere skal konfigureres til at bruge fjernskrivebord, før du kan "
#~ "forbinde til dem."

#~ msgid "We hope that you enjoy Connections!"
#~ msgstr "Vi håber, du er glad for Forbindelser!"

#~ msgid "More information can be found in the help."
#~ msgstr "Yderligere oplysninger kan findes i hjælpen."

#~ msgid "Scaling"
#~ msgstr "Skalering"

#~| msgid "Connections"
#~ msgid "GNOME Connections"
#~ msgstr "GNOME Forbindelser"

#~ msgid "A remote desktop client for the GNOME desktop environment."
#~ msgstr "En fjernskrivebordsklient til GNOME-skrivebordsmiljøet."

#~ msgid "Create a New Connection"
#~ msgstr "Opret en ny forbindelse"

#~ msgid ""
#~ "Enter a machine address to connect to. Address can begin with rdp:// or "
#~ "vnc://"
#~ msgstr ""
#~ "Indtast adressen til en maskine der skal oprettes forbindelse til. "
#~ "Adressen kan begynde med rdp:// eller vnc://"

#~ msgid "Server"
#~ msgstr "Server"

#~ msgid "Create"
#~ msgstr "Opret"
